package com.me.heartdefense;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.files.FileHandle;

public class Main {
	public static void main(String[] args) {
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = "Heart Defense";
		cfg.useGL20 = false;
		cfg.width = 1024;
		cfg.height = 768;
		
		new LwjglApplication(new HeartDefense(), cfg);
		
        FileHandle file = Gdx.files.local("bestscore.txt");
        
        try {
        	file.readString();
        } catch (Exception e) {
			file.writeString("0", false);
		}
	}
}
