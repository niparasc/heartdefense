package com.me.heartdefense.screens;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.me.heartdefense.HeartDefense;
import com.me.heartdefense.utils.DefaultActorListener;

public class MenuScreen extends AbstractScreen {
    public MenuScreen(HeartDefense game) {
        super(game);
    }

    @Override
    public void show() {
        super.show();

        // retrieve the default table actor
        Table table = super.getTable();
        table.add("Welcome to Heart Defense!").spaceBottom(50);
        table.row();

        // register the button "start game"
        TextButton startGameButton = new TextButton("Start game", getSkin());
        startGameButton.addListener(new DefaultActorListener() {
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);
                game.setScreen(new GameScreen(game));
            }
        });
        table.add(startGameButton).size(300, 60).uniform().spaceBottom(10);
        table.row();
    }
}
