package com.me.heartdefense.screens;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.me.heartdefense.HeartDefense;
import com.me.heartdefense.utils.DefaultActorListener;

public class GameOverScreen extends AbstractScreen {

	public static final String gameOver = "Dude, the game is over!";
	
	public GameOverScreen(HeartDefense game) {
		super(game);
	}

    @Override
    public void show() {
        super.show();

        // retrieve the default table actor
        Table table = super.getTable();
        table.defaults().spaceBottom(30);
        table.add(gameOver).colspan(2);

        // register the back button
        TextButton backButton = new TextButton("Back to main menu", getSkin());
        backButton.addListener(new DefaultActorListener() {
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);
//                game.getSoundManager().play(HeartDefenseSound.CLICK);
                game.setScreen(new MenuScreen(game));
            }
        });
        table.row();
        table.add(backButton).size(250, 60).colspan(2);
    }
}
