package com.me.heartdefense.screens;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.delay;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeIn;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeOut;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Scaling;
import com.me.heartdefense.HeartDefense;

/**
 * Shows a splash image and moves on to the next screen.
 */
public class SplashScreen extends AbstractScreen {
    private Image splashImage;

    public SplashScreen(HeartDefense game) {
        super(game);
    }

    @Override
    public void show() {
        super.show();

        // load the texture with the splash image
        Texture splashTexture = new Texture("splash.png");
        
        // set the linear texture filter to improve the image stretching
        splashTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
        
        TextureRegion splashRegion = new TextureRegion(splashTexture, 0, 0, 512, 512);
        
        Drawable splashDrawable = new TextureRegionDrawable(splashRegion);

        // here we create the splash image actor; its size is set when the
        // resize() method gets called
        splashImage = new Image(splashDrawable, Scaling.none);
        splashImage.setFillParent(true);

        // this is needed for the fade-in effect to work correctly; we're just
        // making the image completely transparent
        splashImage.getColor().a = 0f;

        // configure the fade-in/out effect on the splash image
        splashImage.addAction(sequence(
        		fadeIn(0.75f),
        		new Action() {
        			@Override
        			public boolean act(float delta) {
        				
        				return true;
        			}
        		},
        		delay(1.75f),
        		fadeOut(0.75f),
	            new Action() {
	                @Override
	                public boolean act(float delta) {
	                    // the last action will move to the next screen
	                    game.setScreen(new MenuScreen(game));
	                    return true;
	                }
            }));

        // and finally we add the actor to the stage
        stage.addActor(splashImage);
    }
}
