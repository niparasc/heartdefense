package com.me.heartdefense.screens;

import com.badlogic.gdx.Gdx;
import com.me.heartdefense.HeartDefense;
import com.me.heartdefense.view.InputHandler;
import com.me.heartdefense.view.World;
import com.me.heartdefense.view.WorldRenderer;

public class GameScreen extends AbstractScreen {
	private World world;
	private WorldRenderer renderer;
	
	public GameScreen(HeartDefense game) {
		super(game);
		world = new World(game);
		renderer = new WorldRenderer(world);
	}

	public World getWorld() {
		return world;
	}

	public void setWorld(World world) {
		this.world = world;
	}

	public WorldRenderer getRenderer() {
		return renderer;
	}

	public void setRenderer(WorldRenderer renderer) {
		this.renderer = renderer;
	}

    @Override
    public void show() {
    	// set our custom input processor
    	Gdx.input.setInputProcessor(new InputHandler(world));
    }

    @Override
    public void resize(int width, int height) {
    	renderer.resize(width, height);
    	world.resize();
    }

    @Override
    public void render(float delta) {
    	if(world.gameOver()) {
    		game.setScreen(new GameOverScreen(game));
    		return;
    	}
        world.update();
        renderer.render();
    }

    @Override
    public void hide() {
        dispose();
    }

    @Override
    public void pause() {
    	
    }

    @Override
    public void resume() {
    	
    }

    @Override
    public void dispose() {
    	world.dispose();
    	renderer.dispose();
    }	
}
