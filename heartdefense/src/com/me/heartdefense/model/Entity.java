package com.me.heartdefense.model;

import com.badlogic.gdx.math.Vector2;


public class Entity {
    public Vector2 position;
    public float radius;
    
    public Entity(){
        super();
    }
    
    public Entity(Vector2 position, float radius) {
        this.position = position;
        this.radius = radius;
    }
    
    public Vector2 getPosition() {
        return position;
    }

    public void setPosition(Vector2 position) {
        this.position = position;
    }
    
    public void setPosition(float x, float y) {
        this.position.x = x;
        this.position.y = y;
    }
}