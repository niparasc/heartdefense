package com.me.heartdefense.model;

import com.badlogic.gdx.math.Vector2;

public abstract class MovableEntity extends Entity {
       public Vector2 velocity;
       protected float speed;
       protected float rotation;
       
       
       public MovableEntity(){
    	   
       }

       public MovableEntity(Vector2 position, int radius, Vector2 velocity, float speed) {
               super(position, radius);
               this.speed = speed;
               this.velocity = velocity;
       }

       public Vector2 getVelocity() {
               return velocity;
       }

       public void setVelocity(Vector2 velocity) {
               this.velocity = velocity;
       }

       public float getSpeed() {
               return speed;
       }

       public void setSpeed(float speed) {
               this.speed = speed;
       }

       public void update(float delta){
    	   position.x += velocity.x * speed * delta;
    	   position.y += velocity.y * speed * delta;
       }
}