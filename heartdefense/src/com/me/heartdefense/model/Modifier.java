package com.me.heartdefense.model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.math.Vector2;

public class Modifier extends MovableEntity {
	
	public static final int MAX_RADIUS = 48;
	public static final int MIN_RADIUS = 8;
	public static final int MAX_BONUS = 32;
	
	private Vector2 target;
	public int type;
	public int bonus;
	public int keyCode;
	
	
	public Modifier(float target_x, float target_y, int type){
		super();
		this.type = type;
		double size = Math.random(); 
		
		if (type == 0){ // Plus sign
			bonus = (int) Math.round(MAX_BONUS * size);
		} else if (type == 1){ // Minus sign
			bonus = (int) Math.round(-MAX_BONUS * size);
		} else if (type == 2){ // Death!
			size = 0.5;
			bonus = 512; // Kills you!
		}
		
		this.radius = MIN_RADIUS + Math.round(size * (MAX_RADIUS - MIN_RADIUS));
		
        double angle = Math.random() * 2 * Math.PI;
        double distance = Math.sqrt((Math.pow(Gdx.graphics.getWidth() / 2, 2) + Math.pow(Gdx.graphics.getHeight() / 2, 2))) * 1.1;
        target = new Vector2(target_x, target_y);
        position = new Vector2((float)(Math.cos(angle) * distance) + target.x, (float)(Math.sin(angle) * distance) + target.y);
        velocity = new Vector2(target.x - position.x, target.y - position.y);
        velocity.nor();
        
        keyCode = Keys.A + (int) Math.round(Math.random() * (Keys.Z - Keys.A));
        
        speed = 100f; // px/s
	}


	public void setTarget(Vector2 position) {
		target.x = position.x;
		target.y = position.y;
		
		velocity.x = target.x - this.position.x;
		velocity.y = target.y - this.position.y;
        velocity.nor();
	}
}
