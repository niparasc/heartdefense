package com.me.heartdefense.view;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.me.heartdefense.model.Modifier;

public class WorldRenderer {
	private World world;

	private SpriteBatch batch;
	private OrthographicCamera cam;
	private float width;
	private float height;

	private ShapeRenderer sr; // for debugging (collisions)

	BitmapFont font = new BitmapFont();

	// Textures
	Texture plusTexture; // Type 0
	Texture lessTexture; // Type 1
	Texture deathTexture; // Type 2

	Texture heartTexture;
	Texture heartBackgroundTexture;

	ArrayList<Texture> textureList;

	private Color plusColor;
	private Color minusColor;

	public WorldRenderer(World world) {
		this.world = world;

		world.setWorldRenderer(this);

		// width and height to be used by the camera (zoom in-out)
		width = Gdx.graphics.getWidth();
		height = Gdx.graphics.getHeight();

		cam = new OrthographicCamera();
		cam.setToOrtho(false, width, height);
		cam.update();

		batch = new SpriteBatch();
		batch.setProjectionMatrix(cam.combined);

		// Initialize textures
		plusTexture = new Texture("plus.png");
		lessTexture = new Texture("minus.png");
		deathTexture = new Texture("death.png");
		heartTexture = new Texture("heart.png");
		heartBackgroundTexture = new Texture("heart_background.png");

		// Texture filter
		plusTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		lessTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		deathTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);

		heartTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		heartBackgroundTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);

		// Texture list
		textureList = new ArrayList<Texture>();
		textureList.add(plusTexture);
		textureList.add(lessTexture);
		textureList.add(deathTexture);

		plusColor = new Color(0.71f, 0f, 0.02f, 1f);
		minusColor = new Color(0.12f, 0.15f, 0.4f, 1f);

		sr = new ShapeRenderer();
	}

	public void resize(int width, int height) {
		cam.setToOrtho(false, width, height);

		cam.update();
		batch.setProjectionMatrix(cam.combined);
	}

	public void render() {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		// commence rendering
		batch.begin();

		for (int i = 0; i < world.modifierList.size(); i++){
			Modifier m = world.modifierList.get(i);
			Texture t = textureList.get(m.type);
			batch.draw(t, m.position.x - m.radius, m.position.y - m.radius, m.radius * 2, m.radius * 2);

			if (Gdx.app.getType() == ApplicationType.Desktop){
				TextBounds bounds = font.getBounds("" + (char)(65 + (54 - m.keyCode)));
	            font.draw(batch, "" + (char)(65 + (m.keyCode - Keys.A)),
	                    m.position.x - m.velocity.x * (Math.max(m.radius * 1.5f, 0) + bounds.width ),
	                    m.position.y - m.velocity.y * (Math.max(m.radius * 1.5f, 0) + bounds.height));
			}

		}

		batch.draw(heartBackgroundTexture, world.heart.position.x - 64, world.heart.position.y - 64, 128, 128);
		batch.draw(heartTexture, world.heart.position.x - world.heart.radius, world.heart.position.y - world.heart.radius, world.heart.radius * 2, world.heart.radius * 2);

		TextBounds scoreBounds = font.getBounds("Score: " + world.score);
        font.draw(batch, "Score: " + world.score, 2, scoreBounds.height + 2);
        TextBounds bestScoreBounds = font.getBounds("Best score: " + world.bestScore);
        font.draw(batch, "Best score: " + world.bestScore, Gdx.graphics.getWidth() - bestScoreBounds.width - 2, bestScoreBounds.height + 2);

		// done rendering
		batch.end();

		sr.setProjectionMatrix(cam.combined);
        sr.begin(ShapeType.FilledRectangle);
        sr.setColor(plusColor);
        sr.filledRect(0, Gdx.graphics.getHeight(), Gdx.graphics.getWidth(), -Gdx.graphics.getHeight() / 20,
        		plusColor, plusColor, Color.BLACK, Color.BLACK);
        sr.setColor(minusColor);
        // sr.filledRect((((world.heartStatus + 256) / 512f) * Gdx.graphics.getWidth()), Gdx.graphics.getHeight(), Gdx.graphics.getWidth(), -Gdx.graphics.getHeight() / 20);
        sr.filledRect((((world.heartStatus + 256) / 512f) * Gdx.graphics.getWidth()), Gdx.graphics.getHeight(), Gdx.graphics.getWidth(), -Gdx.graphics.getHeight() / 20,
        		minusColor, minusColor, Color.BLACK, Color.BLACK);
        sr.end();

	}

	public OrthographicCamera getCamera() {
		return cam;
	}

	public void dispose() {
		batch.dispose();
		plusTexture.dispose();
		lessTexture.dispose();
		deathTexture.dispose();
		heartTexture.dispose();
		sr.dispose();
	}
}
