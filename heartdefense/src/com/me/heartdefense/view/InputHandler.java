package com.me.heartdefense.view;

import java.util.LinkedList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Application.ApplicationType;
import com.me.heartdefense.model.Modifier;

public class InputHandler implements InputProcessor {
	private World world;
	
	public InputHandler(World world) {
		this.world = world;
	}

	@Override
	public boolean keyDown(int keycode) {
		if (Gdx.app.getType() != ApplicationType.Desktop){
			return false;
		}
        for(Modifier m: world.modifierList){
            if (m.keyCode == keycode){
                world.modifierList.remove(m);
                break;
            }
        }
        return false;
    }

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        int fingerRadius = 0;
        screenY = Gdx.graphics.getHeight() - screenY;
        
        LinkedList<Modifier> deletables = new LinkedList<Modifier>();
        for(Modifier entity: this.world.modifierList){    
            if (entity.position.dst2(screenX, screenY) <= Math.pow(entity.radius + fingerRadius, 2)){
            	deletables.add(entity);
            }
        }
        for(Modifier deletable : deletables){
            this.world.modifierList.remove(deletable);
        }
        return false;
    }

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}
}
