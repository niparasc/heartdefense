package com.me.heartdefense.view;

import java.util.LinkedList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.me.heartdefense.HeartDefense;
import com.me.heartdefense.model.Entity;
import com.me.heartdefense.model.Modifier;
import com.me.heartdefense.services.MusicManager.HeartDefenseMusic;
import com.me.heartdefense.services.SoundManager.HeartDefenseSound;

public class World {
	HeartDefense game;
	WorldRenderer renderer;

	private FileHandle scoreFile;

	protected LinkedList<Modifier> modifierList;
	protected Entity heart;

	// Modifiers spawn
	private static final float initialSpawnRate = 1f;
	private static final float spawnRateModifier = 0.996f;
	private static final float minSpawnRate = 0.1f;

	private float gameSeconds;
	private float lastSpawn;
	private float spawnRate;

	// Death spawn
	private static final float initialDeathSpawnRate = 0.01f;
	private static final float maxDeathSpawnRate = 0.2f;
	private static final float deathSpawnModifier = 0.01f;

	private float deathSpawnRate;

	// Heart beats
	private static final float standardBeatRate = 50f;
	private static final float beatBaseDuration = 0.24f;
	// private static final float beatVariation = 0.1f;

	private float lastBeat;
	private int beatStatus;
	private float beatRate;

	// Heart status
	private static final int baseHeartStatus = 32;
	public float heartStatus;

	public long score;
	public long bestScore;

	public boolean gameOver = false;

	public World(HeartDefense game){
		this.game = game;
		gameSeconds = 0f;
		lastSpawn = 0f;
		heartStatus = baseHeartStatus * (Math.random() < 0.5 ? -1 : 1);
		spawnRate = initialSpawnRate;

		deathSpawnRate = initialDeathSpawnRate;

		lastBeat = 0f;
		beatStatus = 0;
		beatRate = 0f;

		score = 0;
		scoreFile = Gdx.files.local("bestscore.txt");
		bestScore = Integer.parseInt(scoreFile.readString());

		heart = new Entity(new Vector2(Gdx.graphics.getWidth()/2, Gdx.graphics.getHeight()/2), 64f);

		modifierList = new LinkedList<Modifier>();
		game.getMusicManager().play(HeartDefenseMusic.GAMEPLAY);
	}

	public WorldRenderer getWorldRenderer() {
		return renderer;
	}

	public void setWorldRenderer(WorldRenderer renderer) {
		this.renderer = renderer;
	}

	public boolean gameOver() {
		return gameOver;
	}

	public void update() {
		float delta = Gdx.graphics.getDeltaTime(); // Seconds
		gameSeconds += delta;

		float deviation = Math.min(0.05f + (gameSeconds / 3000f), 0.7f);
		heartStatus += heartStatus < 0 ? -deviation : deviation;

		// Spawn
		if (gameSeconds - lastSpawn >= spawnRate){
			lastSpawn = gameSeconds;
			if (spawnRate > minSpawnRate){
				spawnRate *= spawnRateModifier;
			}
			int type;

			if (Math.random() < deathSpawnRate){ // Death!
				if (deathSpawnRate < maxDeathSpawnRate){
					deathSpawnRate += deathSpawnModifier;
				}
				modifierList.add(new Modifier(heart.position.x, heart.position.y, 2));
			}

			type = Math.random() < ((heartStatus / 512f) + 0.5) ? 0 : 1;
			modifierList.add(new Modifier(heart.position.x, heart.position.y, type));
		}

		// Modifiers
		Array<Modifier> deletables = new Array<Modifier>();

		for (int i = 0; i < modifierList.size(); i++){
			Modifier modifier = modifierList.get(i);
			modifier.update(delta);

			if (modifier.position.x - modifier.radius * 1.1 > Gdx.graphics.getWidth() && modifier.velocity.x > 0 ||
				modifier.position.x + modifier.radius * 1.1 < 0 && modifier.velocity.x < 0 ||
				modifier.position.y - modifier.radius * 1.1 > Gdx.graphics.getHeight() && modifier.velocity.y > 0 ||
				modifier.position.y + modifier.radius * 1.1 < 0 && modifier.velocity.y < 0) {
				deletables.add(modifier);
			}
			if (modifier.position.dst2(heart.position) < Math.pow(modifier.radius, 2)){
				heartStatus += modifier.bonus;
				deletables.add(modifier);
			}
		}

		for (int i = deletables.size - 1; i >= 0; i--){
			modifierList.remove(deletables.get(i));
		}

		// Music
		game.getMusicManager().setVolume(Math.max(0f, 0.75f - (Math.abs(heartStatus) / 256.0f)));

		// Beats
		beatRate = standardBeatRate * (heartStatus + 256) / 256f + 20; // Mapped stdBR * [0, 2] + 20
		if (beatStatus == 0 && gameSeconds - lastBeat >= 60f / beatRate){
			lastBeat = gameSeconds;
			beatStatus = 1;
			heart.radius += 10;

			game.getSoundManager().setVolume(Math.min(Math.abs(heartStatus) / 256.0f, 1.0f));

			game.getSoundManager().play(HeartDefenseSound.BEAT);
		}

		if (beatStatus == 1 && gameSeconds - lastBeat >= beatBaseDuration){
			heart.radius -= 10;
			beatStatus = 0;
		}

		score += (256 - Math.abs(Math.round(heartStatus))) / 10;

		// Game over
		if (heartStatus <= -256 || heartStatus >= 256) {
			gameOver = true;

			if (score > bestScore) {
				scoreFile.writeString(String.valueOf(score), false);
			}
		}
	}

	public void dispose() {
		// dispose what's disposable
	}

	public void resize() {
		// Reposition Heart
		heart.setPosition(Gdx.graphics.getWidth()/2, Gdx.graphics.getHeight()/2);

		for (int i = 0; i < modifierList.size(); i++){
			modifierList.get(i).setTarget(heart.position);
		}

	}
}
