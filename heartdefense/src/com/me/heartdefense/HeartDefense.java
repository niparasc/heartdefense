package com.me.heartdefense;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.me.heartdefense.screens.MenuScreen;
import com.me.heartdefense.screens.SplashScreen;
import com.me.heartdefense.services.MusicManager;
import com.me.heartdefense.services.SoundManager;

public class HeartDefense extends Game {

    // constant useful for logging
    public static final String LOG = HeartDefense.class.getSimpleName();

    // version
    public static final String VERSION = "0.0.1";

    // whether we are in development mode
    public static final boolean DEV_MODE = false;

    // services
    private MusicManager musicManager;
    private SoundManager soundManager;

    public HeartDefense() {}


    // Services' getters

    public MusicManager getMusicManager() {
        return musicManager;
    }

    public SoundManager getSoundManager() {
        return soundManager;
    }


    // Game-related methods

    @Override
    public void create() {

        // create the music manager
        musicManager = new MusicManager();
        musicManager.setVolume(1f);
        musicManager.setEnabled(true);

        // create the sound manager
        soundManager = new SoundManager();
        soundManager.setVolume(1f);
        soundManager.setEnabled(true);
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);

        // show the splash screen when the game is resized for the first time;
        // this approach avoids calling the screen's resize method repeatedly
        if(getScreen() == null) {
        	if(DEV_MODE) {
            	setScreen(new MenuScreen(this));
            } else {
            	setScreen(new SplashScreen(this));
            }
        }
    }

    @Override
    public void render() {
        super.render();
    }

    @Override
    public void pause() {
        super.pause();
    }

    @Override
    public void resume() {
        super.resume();
    }

    @Override
    public void setScreen(Screen screen) {
        super.setScreen(screen);
    }

    @Override
    public void dispose() {
        super.dispose();

        // dipose some services
        musicManager.dispose();
        soundManager.dispose();
    }

}
